import React, {Fragment} from 'react';
import {Image, Modal, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View} from 'react-native';
import axios from 'axios';

export default class AppBuilder extends React.Component {
  state = {
    contactList: {},
    selectedId: null,
    modalVisible: false
  }

  setModalVisible(visible){
    this.setState({modalVisible: visible})
  }

  selectId = (id) => {
    this.setState({selectedId: id})
  }


  componentDidMount () {
    axios.get('https://controlwork9.firebaseio.com/contacts.json').then(response => {
      this.setState({contactList: response.data})
    });
  }


  render () {

    let modalContent = null;

    if (this.state.selectedId) {
      modalContent = (
        <Fragment>
          <Text>Name: {this.state.contactList[this.state.selectedId].name}</Text>
          <Image source={{uri: this.state.contactList[this.state.selectedId].url}} style={{width: 50, height: 50, marginRight: 10
          }}/>
          <Text>Tel. number: {this.state.contactList[this.state.selectedId].number}</Text>
          <Text>E-mail: {this.state.contactList[this.state.selectedId].email}</Text>
        </Fragment>
      );
    }

    return (
      <View style={styles.container}>
        <Text style={styles.header}>Contacts</Text>

        {Object.keys(this.state.contactList).map(id => (
          <TouchableOpacity style={styles.contact}
                            onPress={() => {this.setModalVisible(true); this.selectId(id)}} key={id} >
            <View style={styles.picText} >

              <Text>{this.state.contactList[id].name}</Text>
              <Image source={{uri: this.state.contactList[id].url}} style={{width: 50, height: 50, marginRight: 10
              }}/>
            </View>
          </TouchableOpacity>
        ))}

        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
          }}>
          <View style={{marginTop: 22}}>
            <View >
              {modalContent}
              <TouchableHighlight style={styles.back}
                onPress={() => {
                  this.setModalVisible(!this.state.modalVisible);}}>
                <Text style={styles.button} >Back to contacts</Text>
              </TouchableHighlight>
            </View>
          </View>
        </Modal>


      </View>
    )
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    width: '100%',
  },
  picText: {
    width: '100%',
    backgroundColor: '#eee',
    padding: 10,
    marginTop: 10,
  },
  header: {
    marginTop: 60,
    backgroundColor: '#6fa0ee',
    width: '100%', height: '10%',
    fontSize: 26,
    fontWeight: 'bold',
    justifyContent: 'center',
  },
  contact: {
    width: '100%',
    justifyContent: 'center',
    marginLeft: 10,
  },

  back: {
    backgroundColor: '#6fa0ee',
    width: '100%', height: '40%',
    fontSize: 26,
    fontWeight: 'bold',
  },
  button: {
    fontSize: 26,
    fontWeight: 'bold',
  }


});